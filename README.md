
## 简介
```bash
大数据可视化平台
基于Vue + DataV + Echarts开发

大哥们，觉得可以的话麻烦点个Star吧，跪谢
```

项目在线浏览：
 [点击浏览](http://121.40.199.11/)<br/>
![avatar](https://gitee.com/ymdqq/vue-datav-screen/raw/master/src/assets/DP.png)
![avatar](https://gitee.com/ymdqq/vue-datav-screen/raw/master/src/assets/tt0.top-590589.gif)

## Build Setup

```bash

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。
# 可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```
